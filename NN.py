# -*- coding: utf-8 -*-
"""
Created on Fri Nov 12 12:31:26 2021

@author: Lauri Kemppi
"""

import tensorflow as tf
from tensorflow.keras import models, layers, applications, optimizers, preprocessing, callbacks
import matplotlib.pyplot as plt
from sklearn import model_selection, metrics
import cv2
import numpy as np
import os
import io
import itertools
import pickle
from math import log

from tensorflow.python.framework.tensor_spec import TensorSpec

class ObjectDetector():
    def __init__(self, input_dims, log_folder):
        print("Num GPUs Available for TensorFlow: ", len(tf.config.list_physical_devices('GPU')))
        self.all_images = None
        self.all_bboxes = None # bbox format: x_min, y_min, x_max, y_max
        self.class_amount = 0
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.train_dataset = None
        self.val_dataset = None
        self.input_dims = input_dims
        self.tensorboard_callback = callbacks.TensorBoard(log_dir=log_folder, histogram_freq=1, write_graph=True, write_images=True, update_freq='epoch', profile_batch=2, embeddings_freq=1)
        self.file_writer = tf.summary.create_file_writer(log_folder)
        self.using_TF_input_pipeline = False

    def img_generator(self, all_image_paths, bboxes, labels):
        i = 0
        while i < len(all_image_paths):
            # Load and preprocess the image
            image = preprocessing.image.load_img(all_image_paths[i], target_size=self.input_dims)
            image = preprocessing.image.img_to_array(image)
            image = np.array(image, dtype="float32") / 255.0

            # Normalize bounding boxes to range [0,1] by image size
            img = cv2.imread(all_image_paths[i].decode("utf-8"))
            (h, w) = img.shape[:2]

            bbox = np.empty((4), dtype='float32')     
            bbox[0] = float(bboxes[i][0]) / w
            bbox[1] = float(bboxes[i][1]) / h
            bbox[2] = float(bboxes[i][2]) / w
            bbox[3] = float(bboxes[i][3]) / h
            bbox = np.array(bbox, dtype="float32")
            i += 1

            label = np.empty((self.class_amount), dtype="int32")

            yield image, {"class_label": labels[i], "bounding_box": bbox}

    def load_training_data(self, base_folder, test_split, image_filenames, bbox_inputs, label_inputs, visualize_inputs=False):
        if len(image_filenames) != len(bbox_inputs):
            raise ValueError('Training bboxes and file amounts do not match!')

        all_image_paths = [os.path.join(base_folder, f) for f in image_filenames]

        # Create the full dataset

        ds = tf.data.Dataset.from_generator(
            self.img_generator,
            args=[all_image_paths, bbox_inputs, label_inputs],
            output_signature=(
                tf.TensorSpec(shape=(self.input_dims[0], self.input_dims[1], 3), dtype=tf.float32),
                {
                    "class_label": tf.TensorSpec(shape=(self.class_amount), dtype=tf.int32),
                    "bounding_box": tf.TensorSpec(shape=(4), dtype=tf.float32)
                }
            )
        )

        # Split the dataset

        #ds = ds.shuffle(len(all_image_paths))

        val_amount = int(len(all_image_paths) * test_split)
        train_amount = len(all_image_paths) - val_amount
        
        self.train_dataset = ds.take(train_amount).prefetch(tf.data.AUTOTUNE)
        self.val_dataset = ds.take(val_amount).prefetch(tf.data.AUTOTUNE)

        if visualize_inputs:
            for element in ds:
                self.visualize_bbox(element)

        self.using_TF_input_pipeline = True

        return None
    
    def split_dataset(self, test_split):
        # Split into sets
        self.X_train, self.X_test, self.y_train, self.y_test = model_selection.train_test_split(self.all_images, self.all_bboxes, test_size=test_split)
        return None
    
    def create_model(self, learning_rate, print_summary, class_amount):
        self.class_amount = class_amount
        # Load VGG16 model
        vgg = applications.VGG16(weights="imagenet", include_top=False, input_tensor=layers.Input(shape=(self.input_dims[0], self.input_dims[1], 3)))
        
        # Freeze layers for prewarming of new layers
        vgg.trainable = False
        
        # Flatten the max-pooling output of VGG16
        vgg_output = vgg.output
        vgg_output = layers.Flatten()(vgg_output)
        
        # Make a new fully connected bbox output layer
        bbox_output = layers.Dense(128, activation="relu")(vgg_output)
        bbox_output = layers.Dense(64, activation="relu")(bbox_output)
        bbox_output = layers.Dense(32, activation="relu")(bbox_output)
        bbox_output = layers.Dense(4, activation="sigmoid", name="bounding_box")(bbox_output)

        label_output = layers.Dense(512, activation="relu")(vgg_output)
        label_output = layers.Dropout(0.5)(label_output)
        label_output = layers.Dense(512, activation="relu")(label_output)
        label_output = layers.Dropout(0.5)(label_output)
        label_output = layers.Dense(self.class_amount, activation="softmax", name="class_label")(label_output)
        
        # Connect new layers to the frozen model
        self.model = models.Model(inputs=vgg.input, outputs=(bbox_output, label_output))
        
        losses = {
            "class_label": "categorical_crossentropy",
            "bounding_box": IOUAccuracy()
        }

        lossWeights = {
            "class_label": 1.0,
            "bounding_box": 1.0
        }

        opt = optimizers.Adam(learning_rate=learning_rate)
        self.model.compile(loss=losses, optimizer=opt, metrics=["accuracy"], loss_weights=lossWeights, run_eagerly=True)
        if print_summary: self.model.summary()
        
        return None
    
    def load_model(self, model_path):
        self.model = models.load_model(model_path)
    
    def train(self, batch_size, epochs, labelBinarizer, verbosity = 1, model_save_path = None, labels_save_path = None, plot_loss = False, plot_accuracy = False):
        earlystopping = callbacks.EarlyStopping(monitor="val_loss", mode="min", patience=5, restore_best_weights=True)
        #cm_callback = callbacks.LambdaCallback(on_epoch_end=self.log_confusion_matrix)

        if self.using_TF_input_pipeline:
            H = self.model.fit(self.train_dataset.batch(batch_size),
                        validation_data=self.val_dataset.batch(batch_size),
                        batch_size=batch_size,
                        epochs=epochs,
                        verbose=verbosity,
                        callbacks=[earlystopping, self.tensorboard_callback, ]) #cm_callback])
        else:
            H = self.model.fit(self.X_train, self.y_train, 
                            validation_data=(self.X_test, self.y_test),
                            batch_size=batch_size,
                            epochs=epochs,
                            verbose=verbosity,
                            callbacks=[earlystopping, self.tensorboard_callback, ]) #cm_callback])

        if model_save_path is not None:
            self.model.save(model_save_path, save_format="h5")

        if labels_save_path is not None:
            f = open(labels_save_path, "wb")
            f.write(pickle.dumps(labelBinarizer))
            f.close()
        
        try:
            if plot_loss:
                lossNames = ["loss", "class_label_loss", "bounding_box_loss"]
                N = np.arange(0, len(H.history["loss"])) 
                plt.style.use("ggplot")
                (fig, ax) = plt.subplots(3, 1, figsize=(13,13))
                
                for (i, l) in enumerate(lossNames):
                    title = "Loss for {}".format(l) if l != "loss" else "Total loss"
                    ax[i].set_title(title)
                    ax[i].set_xlabel("Epoch #")
                    ax[i].set_ylabel("Loss")
                    ax[i].plot(N, H.history[l], label=l)
                    ax[i].plot(N, H.history["val_" + l], label="val_" + l)
                    ax[i].legend()

                plt.tight_layout()
                #plt.savefig('training_plot.png')
                #plt.show()
        except Exception as e:
            print(e)

                    
        try:
            if plot_accuracy:
                plt.style.use("ggplot")
                plt.figure()
                plt.plot(N, H.history["class_label_accuracy"], label="class_label_train_acc")
                plt.plot(N, H.history["val_class_label_accuracy"], label="val_class_label_acc")
                plt.title("Class Label Accuracy")
                plt.xlabel("Epoch #")
                plt.ylabel("Accuracy")
                plt.legend(loc="lower left")
        except Exception as e:
            print(e)
        
        return None
    
    def predict(self, testing_images_folder, labelBinarizer, sample_frequency = 1):        
        testing_image_paths = [os.path.join(testing_images_folder, f) for f in os.listdir(testing_images_folder)]            

        # Load images in Tensorflow format
        images = []
        for filepath in testing_image_paths:
            if filepath.find(".jpg") == -1:
                continue
            image = preprocessing.image.load_img(filepath, target_size=self.input_dims)
            image = preprocessing.image.img_to_array(image)
            image = np.expand_dims(image, axis=0) # add batch dimension
            images.append(image)
        
        # Convert to numpy arrays and normalize pixel values to range [0,1]
        images = np.array(images, dtype="float32") / 255.0
        images = np.squeeze(images)

        # Make the predictions
        (bbox_predictions, label_predictions) = self.model.predict(images, callbacks=[self.tensorboard_callback])
        
        i = 0
        # Go through all images
        with self.file_writer.as_default():
            for image in images:
                if sample_frequency != 0 and i % sample_frequency == 0:
                    res = self.visualize_prediction(testing_image_paths[i], label_predictions[i], bbox_predictions[i], labelBinarizer, False, "Predicted bounding box")
                    tf.summary.image("Predicted bounding box for image " + str(i), self.img_to_tf(res), step=i)
                i += 1

    def plot_confusion_matrix(self, cm, class_names):
        figure = plt.figure(figsize=(8, 8))
        plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
        plt.title("Confusion matrix")
        plt.colorbar()
        tick_marks = np.arange(len(class_names))
        plt.xticks(tick_marks, class_names, rotation=45)
        plt.yticks(tick_marks, class_names)

        # Compute the labels from the normalized confusion matrix.
        labels = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)

        # Use white text if squares are dark; otherwise black.
        threshold = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            color = "white" if cm[i, j] > threshold else "black"
            plt.text(j, i, labels[i, j], horizontalalignment="center", color=color)

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        return figure

    def log_confusion_matrix(self, epoch, logs):
        # TODO does not work on coordinates. Make to show the predicted categories instead.
        # Use the model to predict the values from the validation dataset.
        test_pred_raw = self.model.predict(self.X_test)
        test_pred = np.argmax(test_pred_raw, axis=1)

        # Calculate the confusion matrix.
        cm = metrics.confusion_matrix(self.y_test, test_pred)
        # Log the confusion matrix as an image summary.
        figure = self.plot_confusion_matrix(cm, class_names=["Lower left", "Upper left", "Lower right", "Upper right"])
        cm_image = self.plot_to_image(figure)

        # Log the confusion matrix as an image summary.
        with self.file_writer.as_default():
            tf.summary.image("Confusion Matrix", cm_image, step=epoch)

    def visualize_prediction(self, image_path, label_pred, bbox, labelBinarizer, raw_coordinates=False, title="Bounding box"):
        # Load the input image (in OpenCV format), and resize it to fit the screen
        image = cv2.imread(image_path)
        label = labelBinarizer.classes_[np.argmax(label_pred, axis=0)]
        #image = imutils.resize(image, width=600)
        (h, w) = image.shape[:2]
        # Scale the bounding box to image
        if not raw_coordinates:
            startX = int(bbox[0] * w)
            startY = int(bbox[3] * h)
            endX = int(bbox[2] * w)
            endY = int(bbox[1] * h)
        else:
            startX = int(bbox[0])
            startY = int(bbox[1])
            endX = int(bbox[2])
            endY = int(bbox[3])            
        # Draw the bounding box and show the image
        cv2.rectangle(image, (startX, startY), (endX, endY), (0, 255, 0))
        y = startY - 10 if startY - 10 > 10 else startY + 10
        cv2.putText(image, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
        #cv2.imshow(title, image)
        return image

    def visualize_bbox(self, data, raw_coordinates=False, title="Bounding box"):
        image = data[0].numpy()
        label = data[1].numpy()
        bbox = data[2].numpy()
        #image = imutils.resize(image, width=600)
        (h, w) = image.shape[:2]
        # Scale the bounding box to image
        if not raw_coordinates:
            startX = int(bbox[0] * w)
            startY = int(bbox[3] * h)
            endX = int(bbox[2] * w)
            endY = int(bbox[1] * h)
        else:
            startX = int(bbox[0])
            startY = int(bbox[1])
            endX = int(bbox[2])
            endY = int(bbox[3])            
        # Draw the bounding box and show the image
        cv2.rectangle(image, (startX, startY), (endX, endY), (0, 255, 0))
        cv2.imshow(title, image)
        cv2.waitKey(1000)
        cv2.destroyAllWindows()
        return image

    def plot_to_image(self, figure):
        """Converts the matplotlib plot specified by 'figure' to a PNG image and
        returns it. The supplied figure is closed and inaccessible after this call."""
        # Save the plot to a PNG in memory.
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        # Closing the figure prevents it from being displayed directly inside
        # the notebook.
        plt.close(figure)
        buf.seek(0)
        # Convert PNG buffer to TF image
        image = tf.image.decode_png(buf.getvalue(), channels=4)
        # Add the batch dimension
        image = tf.expand_dims(image, 0)
        return image

    def img_to_tf(self, img):  
        # Save the img to a PNG in memory.
        is_success, buffer = cv2.imencode(".png", img)
        buf = io.BytesIO(buffer)
        buf.seek(0)
        # Convert PNG buffer to TF image
        image = tf.image.decode_png(buf.getvalue(), channels=4)
        # Add the batch dimension
        image = tf.expand_dims(image, 0)
        return image

class IOUAccuracy(tf.keras.losses.Loss):
    def __init__(self):
        super().__init__()

    def call(self, true_bboxes, predicted_bboxes):
        # determine x and y coordinates of intersection
        true_bboxes = true_bboxes.numpy()
        predicted_bboxes = predicted_bboxes.numpy()
        iou = np.zeros(true_bboxes.shape[0])

        for i in range(0,true_bboxes.shape[0]):
            true_bbox = true_bboxes[i]
            predicted_bbox = predicted_bboxes[i]
            x_min = max(true_bbox[0], predicted_bbox[0])
            y_min = max(true_bbox[1], predicted_bbox[1])
            x_max = min(true_bbox[2], predicted_bbox[2])
            y_max = min(true_bbox[3], predicted_bbox[3])

            # compute the area of intersection rectangle
            interArea = max(0, x_max - x_min) * max(0, y_max - y_min)

            # if there is no overlap
            if interArea == 0:
                new_iou = 15

            else:
                # computer the areas of true bbox and predicted bbox
                true_bboxArea = (true_bbox[2] - true_bbox[0]) * (true_bbox[3] - true_bbox[1])
                predicted_bboxArea = (predicted_bbox[2] - predicted_bbox[0]) * (predicted_bbox[3] - predicted_bbox[1])

                # compute negative natural log of intersection over union
                new_iou = -log(interArea / float(true_bboxArea + predicted_bboxArea - interArea))
            
            iou[i] = new_iou

        # return the intersection over union value
        #iou = tf.constant(int(iou))
        return tf.constant(iou)
