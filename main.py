# -*- coding: utf-8 -*-
"""
Created on Mon Nov 15 08:31:54 2021

@author: Lauri Kemppi

Python3
Installation: Tensorflow + matplotlib + sklearn + cv2 + numpy + panda3d
"""

import os
import sys
import getopt
import subprocess
import time
from datetime import datetime
import webbrowser
import signal
from panda import PandaInputCreator
from NN import ObjectDetector
from numpy import loadtxt, asarray
from sklearn.preprocessing import LabelBinarizer
import pickle

## NEEDED PARAMETERS
TRAINING_MODELS_FOLDER = "rpw_models"
TRAINING_IMAGES_FOLDER = "rpw_training_images" # THIS FOLDER WILL BE RESET ON IMAGE RENDERING (runmode 0 or 1). All folders starting with this name and containing the necessary files are included in the training datasets.
TRAINING_BACKGROUNDS_FOLDER = "rpw_background_images"
TESTING_IMAGES_FOLDER = "rpw_testing_images"
TEST_DATA_FOLDER = "rpw_tests"

## ADDITIONAL PARAMETERS
TRAINING_BBOXES_FILENAME = "training_bboxes.csv" # Located in TRAINING_IMAGES_FOLDER
TRAINING_CLASSES_FILENAME = "training_classes.txt" # Located in TRAINING_IMAGES_FOLDER
TRAINING_IMAGE_FILENAMES_FILENAME = "training_images.txt" # Located in TRAINING_IMAGES_FOLDER
MODEL_SAVE_FILENAME = "model.h5" # Located in TEST_DATA_FOLDER
MODEL_LOAD_FILENAME = "model.h5" # Located in TEST_DATA_FOLDER
LABELS_SAVE_FILENAME = "labels.pickle" # Located in TEST_DATA_FOLDER
LABELS_LOAD_FILENAME = "labels.pickle" # Located in TEST_DATA_FOLDER
TENSORBOARD_SUBFOLDER = "tensorboard" # Located in TEST_DATA_FOLDER

# Datetime stamped paths and variables
MODEL_SAVE_FILENAME = datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + "_" + MODEL_SAVE_FILENAME
LABELS_SAVE_FILENAME = datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + "_" + LABELS_SAVE_FILENAME
TENSORBOARD_SAVE_FOLDER = datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + "_" + TENSORBOARD_SUBFOLDER

# Path variables from parameters
BASE_PATH = os.getcwd()
TRAINING_MODELS_PATH = os.path.join(BASE_PATH, TRAINING_MODELS_FOLDER)
TRAINING_IMAGES_PATH = os.path.join(BASE_PATH, TRAINING_IMAGES_FOLDER)
TRAINING_BACKGROUNDS_PATH = os.path.join(BASE_PATH, TRAINING_BACKGROUNDS_FOLDER)
TESTING_IMAGES_PATH = os.path.join(BASE_PATH, TESTING_IMAGES_FOLDER)
TRAINING_BBOXES_PATH = os.path.abspath(TRAINING_IMAGES_PATH + "/" + TRAINING_BBOXES_FILENAME)
TRAINING_CLASSES_PATH = os.path.abspath(TRAINING_IMAGES_PATH + "/" + TRAINING_CLASSES_FILENAME)
TRAINING_IMAGE_FILENAMES_PATH = os.path.abspath(TRAINING_IMAGES_PATH + "/" + TRAINING_IMAGE_FILENAMES_FILENAME)
TEST_DATA_PATH = os.path.join(BASE_PATH, TEST_DATA_FOLDER)
MODEL_SAVE_PATH = os.path.abspath(TEST_DATA_PATH + "/" + MODEL_SAVE_FILENAME)
MODEL_LOAD_PATH = os.path.abspath(TEST_DATA_PATH + "/" + MODEL_LOAD_FILENAME)
LABELS_SAVE_PATH = os.path.abspath(TEST_DATA_PATH + "/" + LABELS_SAVE_FILENAME)
LABELS_LOAD_PATH = os.path.abspath(TEST_DATA_PATH + "/" + LABELS_LOAD_FILENAME)
TEST_TENSORBOARD_PATH = os.path.join(TEST_DATA_PATH, TENSORBOARD_SAVE_FOLDER)

# CLI argument default values
RUN_MODE = 0 # 0 = both Panda and NN, 1 = only Panda, 2 = only NN with training, 3 = only NN predictions
USE_PRETRAINED_MODEL = False
IMAGES_TO_CREATE = 100
NUM_EPOCHS = 3
SHOW_TENSORBOARD = True

# Neural network properties
TEST_SPLIT = 0.20
INPUT_DIMS = (224, 224)
INIT_LR = 1e-4
BATCH_SIZE = 128

# Training data viewing
MODEL_SUMMARY = True
VERBOSITY = 1
PLOT_LOSS = True
PLOT_ACCURACY = True

# Testing data viewing
SAMPLE_FREQUENCY = 1

# Create needed folders
if not os.path.exists(TRAINING_MODELS_PATH):
    os.mkdir(TRAINING_MODELS_PATH)

if not os.path.exists(TRAINING_IMAGES_PATH):
    os.mkdir(TRAINING_IMAGES_PATH)

if not os.path.exists(TRAINING_BACKGROUNDS_PATH):
    os.mkdir(TRAINING_BACKGROUNDS_PATH)    

if not os.path.exists(TESTING_IMAGES_PATH):
    os.mkdir(TESTING_IMAGES_PATH)

if not os.path.exists(TEST_DATA_PATH):
    os.mkdir(TEST_DATA_PATH)

if not os.path.exists(TEST_TENSORBOARD_PATH):
    os.mkdir(TEST_TENSORBOARD_PATH)



def set_args(argv):
    arguments = [None, None, None, None, None]
    try:
        opts, args = getopt.getopt(argv,"hr:m:i:e:t:",["runmode=", "useExistingModel=", "imagesToCreate=", "epochs=", "tensorboard="])
    except getopt.GetoptError:
        print("main.py -r <runmode (0 = full, 1 = panda, 2 = neural network, 3 = only neural network predictions)> -m <useExistingModel (0 = no, 1 = yes) -i <imagesToCreate (number of images to render per object)> -e <epochs (number of epochs to teach)> -t <tensorboard (show tensorboard, 0 = no, 1 = yes)>")
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == "-h":
            print("main.py -r <runmode (0 = full, 1 = panda, 2 = neural network, 3 = only neural network predictions)> -m <useExistingModel (0 = no, 1 = yes) -i <imagesToCreate (number of images to render per object)> -e <epochs (number of epochs to teach)> -t <tensorboard (show tensorboard, 0 = no, 1 = yes)>")
            sys.exit()
        elif opt in ("-r", "--runmode"):
            arg = int(arg)
            if arg == 0 or arg == 1 or arg == 2 or arg == 3:
                arguments[0] = arg
            else:
                print("main.py -r <runmode (0 = full, 1 = panda, 2 = neural network, 3 = only neural network predictions)> -m <useExistingModel (0 = no, 1 = yes) -i <imagesToCreate (number of images to render per object)> -e <epochs (number of epochs to teach)> -t <tensorboard (show tensorboard, 0 = no, 1 = yes)>")
                sys.exit(2)
                
        elif opt in ("-m", "--useExistingModel"):
            arg = int(arg)
            if arg == 0 or arg == 1:
                arguments[1] = arg
            else:
                print("main.py -r <runmode (0 = full, 1 = panda, 2 = neural network with training, 3 = only neural network predictions)> -m <useExistingModel (0 = no, 1 = yes) -i <imagesToCreate (number of images to render per object)> -e <epochs (number of epochs to teach)> -t <tensorboard (show tensorboard, 0 = no, 1 = yes)>")
                sys.exit(2)
                
        elif opt in ("-i", "--imagesToCreate"):
            arg = int(arg)
            arguments[2] = arg
                
        elif opt in ("-e", "--epochs"):
            arg = int(arg)
            arguments[3] = arg

        elif opt in ("-t", "--tensorboard"):
            arg = int(arg)
            arguments[4] = arg
                
    return arguments

def load_classes():
    all_subfolders = os.listdir(BASE_PATH)
    training_classes = None

    for folder in all_subfolders:
        if not folder.startswith(TRAINING_IMAGES_FOLDER):
            continue

        classes_path = os.path.abspath(folder + "/" + TRAINING_CLASSES_FILENAME)
        
        if training_classes is None:
            training_classes = list(loadtxt(classes_path, delimiter=' ', dtype='str'))
        else:
            training_classes = training_classes + list(loadtxt(classes_path, delimiter=' ', dtype='str'))

    lb = LabelBinarizer()
    labels = lb.fit_transform(asarray(training_classes))
    return labels, lb

def load_training_base_data():
    all_subfolders = os.listdir(BASE_PATH)
    training_filenames = None
    training_bboxes = None

    for folder in all_subfolders:
        if not folder.startswith(TRAINING_IMAGES_FOLDER):
            continue

        filenames_path = os.path.abspath(folder + "/" + TRAINING_IMAGE_FILENAMES_FILENAME)
        bboxes_path = os.path.abspath(folder + "/" + TRAINING_BBOXES_FILENAME)

        if training_filenames is None:
            training_filenames = list(loadtxt(filenames_path, delimiter=' ', dtype='str'))
        else:
            training_filenames = training_filenames + list(loadtxt(filenames_path, delimiter=' ', dtype='str'))

        if training_bboxes is None:
            training_bboxes = list(loadtxt(bboxes_path, delimiter=',', dtype='int'))
        else:
            training_bboxes = training_bboxes + list(loadtxt(bboxes_path, delimiter=',', dtype='int'))

    return asarray(training_filenames), asarray(training_bboxes)

if __name__ == "__main__":
    arguments = set_args(sys.argv[1:])
    if arguments[0] is not None:
        RUN_MODE = arguments[0]
    if arguments[1] is not None:
        USE_PRETRAINED_MODEL = bool(arguments[1])
    if arguments[2] is not None:
        IMAGES_TO_CREATE = int(arguments[2])
    if arguments[3] is not None:
        NUM_EPOCHS = int(arguments[3])
    if arguments[4] is not None:
        SHOW_TENSORBOARD = bool(arguments[4])

    print("Starting the program.")
    print()
    
    inputCreator = None
    detector = None
    proc = None

    if RUN_MODE == 0 or RUN_MODE == 1:
        print("Starting the Panda training image creation.")
        print()
        inputCreator = PandaInputCreator(TRAINING_IMAGES_FOLDER, TRAINING_IMAGES_PATH, TRAINING_MODELS_FOLDER, TRAINING_BACKGROUNDS_FOLDER)

        print()
        print("Panda ready, creating " + str(IMAGES_TO_CREATE) + " training images.")
        print()
        while inputCreator.getImageAmount() <= IMAGES_TO_CREATE:
            inputCreator.taskMgr.step()

        print()
        print()
        print("Training images created successfully.")
        print("Saving training image metadata.")
        inputCreator.savebboxes(TRAINING_BBOXES_PATH)
        inputCreator.saveClasses(TRAINING_CLASSES_PATH)
        inputCreator.saveFileNames(TRAINING_IMAGE_FILENAMES_PATH)
        inputCreator.close()
        print()
        print("Training image creation successful.")
        print()
        print()
    
    if RUN_MODE == 0 or RUN_MODE == 2 or RUN_MODE == 3:
        print("Starting the neural network.")
        print()
        detector = ObjectDetector(INPUT_DIMS, TEST_TENSORBOARD_PATH)
        print()

        if USE_PRETRAINED_MODEL:
            print("Loading the existing neural network.")
            detector.load_model(MODEL_LOAD_PATH)
            lb = pickle.loads(open(LABELS_LOAD_PATH, "rb").read())
            print("Existing neural network loaded.")
            print()

        elif not USE_PRETRAINED_MODEL:
            print("Creating a new neural network model.")
            labels, lb = load_classes()
            detector.create_model(INIT_LR, MODEL_SUMMARY, len(lb.classes_))
            print()

        if RUN_MODE != 3:
            print("Loading the training data.")
            labels, lb = load_classes()
            training_filenames, training_bboxes = load_training_base_data()
            detector.load_training_data(BASE_PATH, TEST_SPLIT, training_filenames, training_bboxes, labels)
            print("Training data loaded.")
            print()

            print("Training the neural network.")
            print()
            detector.train(BATCH_SIZE, NUM_EPOCHS, lb, VERBOSITY, MODEL_SAVE_PATH, LABELS_SAVE_PATH, PLOT_LOSS, PLOT_ACCURACY)
            print()

            print("Neural network ready.")
            print()

        print("Making predictions for the given testing images.")
        print()
        detector.predict(TESTING_IMAGES_PATH, lb, SAMPLE_FREQUENCY)

        print("Predictions complete.")
        print()

        if SHOW_TENSORBOARD:
            print()
            print("Showing Tensorboard. If the web page is empty, please refresh it.")
            print()
            proc = subprocess.Popen(["tensorboard", "--logdir=" + TEST_DATA_PATH])
            time.sleep(10)
            webbrowser.open("http://localhost:6006")

    print()
    print()
    print("Finished.")
    input("Press Enter to stop the script.")
    if proc is not None:
        os.kill(proc.pid, signal.CTRL_C_EVENT)
        proc.kill()
