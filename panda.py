# -*- coding: utf-8 -*-
"""
Created on Mon Nov 15 08:25:58 2021

@author: Lauri Kemppi
"""

### INPUT IMAGE CREATION

import time
import numpy as np
from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from panda3d.core import PointLight, VBase4, LPoint3f, LPoint2f
from math import tan, pi
from direct.gui.OnscreenImage import OnscreenImage
import cv2
import os, shutil

class PandaInputCreator(ShowBase):
    def __init__(self, images_folder, images_path, models_folder, background_pics_folder):
        ShowBase.__init__(self)
        #ShowBase.__init__(self, windowType='offscreen')
        
        self.images_folder = images_folder
        self.images_path = images_path
        self.models_folder = models_folder
        self.backgroundpics_folder = background_pics_folder
        
        for filename in os.listdir(images_folder):
            file_path = os.path.join(images_folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

        
        self.bpictures = []
        for filename in os.listdir(self.backgroundpics_folder):
            self.bpictures.append(self.backgroundpics_folder + "/" + filename)
            
        self.imageobject = OnscreenImage(parent=render2dp, image=self.bpictures[0])
        self.cam2dp.node().getDisplayRegion(0).setSort(-20)
            
        # Load all models in folder
        self.models = []
        self.model_names = []
        for filename in os.listdir(models_folder):
            self.models.append(self.loader.loadModel(models_folder + "/" + filename))
            self.model_names.append(os.path.splitext(filename)[0])

        if len(self.models) < 1:
            raise Exception("No models found in given folder: " + models_folder + "!!!") 
        
        # load model and modify it
        for model in self.models:
            self.model = model
            self.model.setColor(1, 1, 1, 1)
            self.model.reparentTo(self.render)
            self.model.setScale(0.05, 0.05, 0.05)
            self.model.setPos(0, -10, 0)

        self.current_model_index = len(self.models) - 1

        # add task that is called
        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")
        self.start_time = time.time()

        # amount of pics taken
        self.amount = 0

        self.disable_mouse()
        self.turned = False
        
        #self.model.show_tight_bounds()

        self.bboxes = []
        self.classes = []
        self.image_files = []
        
    def background_pic(self, imagepath):
        self.imageobject.setImage(imagepath)
        self.cam2dp.node().getDisplayRegion(0).setSort(-20)
        

    def create_lighting(self):
        # create lighting
        r = np.random.rand()
        g = np.random.rand()
        b = np.random.rand()
        for i in range(3):
            for j in range(3):
                if i != 1 and j != 1:
                    plight = PointLight('plight')
                    plight.setColor(VBase4(r, g, b, 0.7))
                    plight.attenuation = (1, 0, 0.01)
                    plightNodePath = self.render.attachNewNode(plight)
                    plightNodePath.setPos(i * 10 - 10, j * 10 - 10, 10)
                    self.render.setLight(plightNodePath)

    def spinCameraTask(self, task):
        self.camera.setPos(0, 0, 0)
        self.camLens.setFov((45, 40))

        if self.amount > 0:
            #time.sleep(0.1)

            # get the tight bounds to create the bounding box
            model_bounds = self.model.get_tight_bounds()
            bbox_points = []
            for i in range(2):
                for j in range(2):
                    for k in range(2):
                        bbox_points.append(LPoint3f(model_bounds[i][0], model_bounds[j][1], model_bounds[k][2]))
            #print(model_bounds)

            x = []
            y = []

            # take the screenshot
            # store the projected x and y coordinates of the 3d bounding box
            img_name = self.images_folder + "/pic" + str(self.amount) + ".jpg"
            self.screenshot(img_name, defaultFilename=False)
            
            point_num = 0
            point_nums = []
            for point in bbox_points:
                pointti = LPoint2f(0, 0)
                if self.camLens.project(point, pointti):

                    x.append(np.interp(pointti[0], [-1, 1], [0, 800]))
                    y.append(600 - np.interp(pointti[1], [-1, 1], [0, 600]))

                else:
                    point_nums.append(point_num)

                point_num += 1
            if len(point_nums) != 0:
                point_nums = np.array(point_nums)
                if np.count_nonzero((point_nums + 1) % 2 == 0) >= 2:
                    if 1 in point_nums and 5 in point_nums:
                        y.append(0)
                    if 3 in point_nums and 7 in point_nums:
                        y.append(0)

                if np.count_nonzero((point_nums + 1) % 2 == 1) >= 2:
                    if 0 in point_nums and 4 in point_nums:
                        y.append(599)
                    if 2 in point_nums and 6 in point_nums:
                        y.append(599)

                if np.count_nonzero(point_nums < 4) >= 2:
                    if 0 in point_nums and 1 in point_nums:
                        x.append(0)
                    if 2 in point_nums and 3 in point_nums:
                        x.append(0)

                if np.count_nonzero(point_nums >= 4) >= 2:
                    if 4 in point_nums and 5 in point_nums:
                        x.append(799)
                    if 6 in point_nums and 7 in point_nums:
                        x.append(799)

            # Testing: draw the bounding box
            #img = cv2.imread(img_name)
            #cv2.rectangle(img, (int(np.array(x).min()), int(np.array(y).min())), (int(np.array(x).max()), int(np.array(y).max())), (0, 0, 255))
            #cv2.imwrite(img_name, img)
            
            # take the min and max values of x and y coordinates to create
            # the 2d bounding box
            self.bboxes.append([int(np.array(x).min()), int(np.array(y).min()), int(np.array(x).max()), int(np.array(y).max())])
            self.image_files.append(img_name)

            self.classes.append(self.model_names[self.current_model_index])

            # store the new start time so that the model can turn and be
            # rendered in time for the screenshot and bbox
            self.start_time = time.time()

            print("Images rendered: " + str(self.amount), end='\r')

        ind = int(np.random.randint(0, len(self.models)))
        self.model.setPos(0, -10, 0)
        self.model = self.models[ind]
        self.current_model_index = ind

        y = np.random.rand()*10+5
        x = np.random.rand() * tan(20/180*pi)*2*y - tan(20/180*pi)*y
        z = np.random.rand() * tan(20/180*pi)*2*y - tan(20/180*pi)*y

        self.model.setHpr(np.random.rand()*360, np.random.rand()*360, np.random.rand()*360)
        self.model.setPos(x, y, z)
        self.render.clearLight()
        self.create_lighting()
        
        self.background_pic(self.bpictures[int(np.random.randint(0, len(self.bpictures)))])

        self.amount += 1

        return Task.cont
    
    def getBoundingBoxes(self):
        return self.bboxes
    
    def getImageAmount(self):
        return self.amount

    def savebboxes(self, path):
        # define data
        data = np.asarray(self.bboxes)
        # save to csv file
        np.savetxt(path, data, delimiter=',')
        return None

    def saveClasses(self, path):
        # define data
        data = np.asarray(self.classes)
        print(data[0])
        # save to csv file
        np.savetxt(path, data, delimiter=' ', fmt='%s')
        return None
    
    def getImageFileNames(self):
        return self.image_files
    
    def saveFileNames(self, path):
        # define data
        data = np.asarray(self.image_files)
        # save to csv file
        np.savetxt(path, data, delimiter=' ', fmt='%s') 
        return None

    def close(self):
        try:
            self.userExit()
        except SystemExit as e:
            print()
        return None


